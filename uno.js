const pemain = [
    {
        team: 'A',
        players: ['@Frenchfriesenak', '@malesinbanget', '@GFKimSeokjin'],
        menang: null
    },
    {
        team: 'B',
        players: ['@kkybox', '@matahariteletabis', '@nadainsn'],
        menang: null
    },
    {
        team: 'C',
        players: ['@gosahbawel', '@blublum', '@Cleopartyz'],
        menang: null
    },
    {
        team: 'D',
        players: ['@ilmasav', '@nanasasem', '@bihuna'],
        menang: null
    },
    {
        team: 'E',
        players: ['@dimsumartabak', '@FYAllIDC', '@Lalalana7'],
        menang: null
    },
    {
        team: 'F',
        players: ['@glukosa', '@yakultkult', '@paris_gun'],
        menang: null
    },
]

const duel = [
    {
        duel: '1',
        menang: null,
        kalah: null
    },
    {
        duel: '2',
        menang: null,
        kalah: null
    },
    {
        duel: '3',
        menang: null,
        kalah: null
    }
];


function eventUno(ctx) {
    if (ctx.message.text.includes('call tim')) { //call tim
        let str = ctx.message.text;
        let tim = str.substring(10, 9).toUpperCase();
        for (let i = 0; i < pemain.length; i++) {
            if (pemain[i].team === tim) {
                pemain[i].players.forEach((element, index) => {
                    ctx.reply((index + 1) + '. ' + element);
                });
            }
        }
    }
    if (ctx.message.text.includes('winner tim')) { //penyisihan 1
        let str = ctx.message.text;
        let tim = str.substring(12, 11).toUpperCase();
        let winner = str.substring(13).trim();
        for (let i = 0; i < pemain.length; i++) {
            if (pemain[i].team === tim) {
                if (pemain[i].menang === null) {
                    let isTeam = false;
                    pemain[i].players.forEach((element, index) => {
                        if (element === winner) {
                            isTeam = true;
                        }
                    })
                    if (isTeam) {
                        pemain[i].menang = winner;
                        ctx.reply('pemenang tim ' + pemain[i].team + ' adalah ' + pemain[i].menang);
                    } else {
                        ctx.reply(winner + ' bukan tim ' + pemain[i].team);
                    }
                } else {
                    ctx.reply('gak, yang menang ' + pemain[i].menang);
                }
            }
        }
    }

    if (ctx.message.text.includes('get tim winner')) { //dapetin daftar pemenang
        pemain.forEach(element => {
            if (element.menang !== null) {
                ctx.reply('pemenang tim ' + element.team + ' adalah ' + element.menang);
            }
        });
    }

    if (ctx.message.text.includes('start duel')) { //penyisihan 2
        let isDoneTim = true;
        pemain.forEach(element => {
            if (element.menang === null) {
                isDoneTim = false;
                ctx.reply('penyisihan pertama belom kelar, tim ' + element.team + ' belom ada juaranya');
            } else {
                // ctx.reply('pemenang tim ' + element.team + ' adalah ' + element.menang);
            }
        });
        if (isDoneTim === true) {
            ctx.reply(pemain[3].menang + ' vs ' + pemain[1].menang);
            ctx.reply(pemain[2].menang + ' vs ' + pemain[0].menang);
            ctx.reply(pemain[4].menang + ' vs ' + pemain[5].menang);
        }
    }

    if (ctx.message.text.includes('winner duel')) { //penyisihan 2
        let str = ctx.message.text;
        let tim = str.substring(13, 12).toUpperCase();
        let winner = str.substring(14).trim();
        for (let i = 0; i < duel.length; i++) {
            if (tim === duel[i].duel) {
                duel[i].menang = winner;
                ctx.reply('pemenang duel ' + duel[i].duel + ' adalah ' + winner);
            }
        }
    }

    if (ctx.message.text.includes('loser duel')) { //penyisihan 2
        let str = ctx.message.text;
        let tim = str.substring(12, 11).toUpperCase();
        let loser = str.substring(13).trim();
        for (let i = 0; i < duel.length; i++) {
            if (tim === duel[i].duel) {
                duel[i].kalah = loser;
                ctx.reply('kalah duel ' + duel[i].duel + ' adalah ' + loser);
            }
        }
    }

    if (ctx.message.text.includes('start final')) { //final
        let final = [];
        duel.forEach(element => {
            if (element.menang !== null) {
                final.push(element.menang);
            }
        });
        ctx.reply(final[0] + ' vs ' + final[1] + ' vs ' + final[2]);

    }

    if (ctx.message.text.includes('start 4')) { //final
        let juara4 = [];
        duel.forEach(element => {
            if (element.kalah !== null) {
                juara4.push(element.kalah);
            }
        });
        ctx.reply(juara4[0] + ' vs ' + juara4[1] + ' vs ' + juara4[2]);

    }
}