const { Telegraf } = require('telegraf');
const moment = require('moment');
const fs = require('fs');
let kakayebot = '990476503:AAFMnVYpJv8h-3neyAZzYuMFVq5Q8qQe4Q4';
const bot = new Telegraf(kakayebot);

let durasi = 0;
let timeCount;
const receptionistInaGroup = -1001436255360;
const ootInaGroup = -1001224424782;
const belajarGroup = -359240354;
const greatdayUX = -1001261132213;
const unoinaGroup = -1001074726623;
const wwinaGroup = -1001359087899;
const admininaGroup = -1001352685899;
const belajarChannel = -1001248651278;
const eventGroup = -1001443181548;
const inaChannel = -1001486393669;
const timUX = '@imam_mm @RindraPSH @Fattahnr @kkybox';
let privateRoomId = [];
let musik = null;
let user = [];
let togglePanggil = 0;
let admin = [];
let untag = [];
let toggleuntag = 0;
let matchww = [];
let matchplayerww = [];
let totalplayerww = [];
let allShio = [
    {
        icon: '🐀',
        shio: 'Tikus',
    },
    {
        icon: '🐃',
        shio: 'Kerbau',
    },
    {
        icon: '🐅',
        shio: 'Harimau',
    },
    {
        icon: '🐇',
        shio: 'Kelinci',
    },
    {
        icon: '🐉',
        shio: 'Naga',
    },
    {
        icon: '🐍',
        shio: 'Ular',
    },
    {
        icon: '🐎',
        shio: 'Kuda',
    },
    {
        icon: '🐐',
        shio: 'Kambing',
    },
    {
        icon: '🐒',
        shio: 'Monyet',
    },
    {
        icon: '🐓',
        shio: 'Ayam',
    },
    {
        icon: '🐕',
        shio: 'Anjing',
    },
    {
        icon: '🐖',
        shio: 'Babi',
    },
]
const commandList = [
    {
        type: 'backup',
        data: [
            {
                cmd: '/getdoc',
                ket: 'generate txt file on server to backup user <code>/panggil</code>'
            },
            {
                cmd: '/bacauser',
                ket: 'read txt file on server to restore user <code>/panggil</code>'
            },
            {
                cmd: '/getadmin',
                ket: 'generate txt file on server to backup admin <code>/listadmin</code>'
            },
            {
                cmd: '/bacaadmin',
                ket: 'read txt file on server to restore admin <code>/listadmin</code>'
            },
            {
                cmd: '/bacauntag',
                ket: 'read txt file on server to restore untag user <code>/untag</code>'
            }
        ]
    },
    {
        type: 'werewolf & werewolf event',
        data: [
            {
                cmd: '/afkww @username',
                ket: 'menambahkan status AFK dan mengurangi 2 point pada user'
            },
            {
                cmd: '/writeww (reply ke hasil permainan)',
                ket: 'mencatat statistik permainan dan poin user'
            },
            {
                cmd: '/pesertaww',
                ket: 'melihat pemain yang sudah terdaftar'
            },
            {
                cmd: '/getwwstat',
                ket: 'menampilkan statistik semua user selama event berlangsung'
            },
            {
                cmd: '/getmystat',
                ket: 'menampilkan statistik user selama event berlangsung'
            },
            {
                cmd: '/tempban (reply ke user)',
                ket: 'melakukan temporary ban pada user'
            },
            {
                cmd: '/ban (reply ke user)',
                ket: 'melakukan ban permanent pada user'
            },
            {
                cmd: '/shiolist',
                ket: 'menampilkan daftar shio untuk keperluan event'
            }
        ]
    },
    {
        type: 'other',
        data: [
            {
                cmd: '/panggil',
                ket: 'mention semua user yang terdaftar pada bot ini'
            },
            {
                cmd: '/untag',
                ket: 'menghapus user dari dalam daftar mention <code>/panggil</code>'
            },
            {
                cmd: '/listadmin',
                ket: 'menampilkan daftar admin group alexis'
            },
            {
                cmd: '/truth',
                ket: 'mengeluarkan pertanyaan truth (ToD)'
            },
            {
                cmd: '/dare',
                ket: 'mengeluarkan perintah dare (ToD)'
            },
            {
                cmd: '/wait detik',
                ket: 'melakukan countdown sekian detik'
            },
            {
                cmd: '/clearwait',
                ket: 'mereset countdown'
            }
        ]
    },
    {
        type: 'game',
        data: [
            {
                cmd: '/startTOD',
                ket: 'Memulai sesi permainan Truth or Dare'
            },
            {
                cmd: '/joinTOD',
                ket: 'Bergabung ke dalam sesi permainan Truth or Dare'
            },
            {
                cmd: '/closeTOD',
                ket: 'Menutup sesi permainan Truth or Dare, user lain tidak dapat <code>/joinTOD</code>'
            },
            {
                cmd: '/openTOD',
                ket: 'Membuka sesi permainan Truth or Dare, user lain dapat <code>/joinTOD</code>'
            },
            {
                cmd: '/clearTOD',
                ket: 'Mengakhiri sesi permainan Truth or Dare'
            },
            {
                cmd: '@kakayebot (inline query)',
                ket: 'untuk menulis pesan rahasia yang hanya bisa dibaca oleh pemain TOD'
            },
        ]
    }
]

bot.command('bacapeserta', ctx => {
    fs.readFile('pesertaww12February2021.txt', 'utf8', (err, data) => {
        userEventWW = JSON.parse(data);
    });
})

bot.command('bacapoint', ctx => {
    fs.readFile('pointww13February2021.txt', 'utf8', (err, data) => {
        totalplayerww = JSON.parse(data);
    });
})

function writePeserta() {
    const date = moment().format('DDMMMMYYYY')
    fs.writeFile('pesertaww' + date + '.txt', JSON.stringify(userEventWW, null, 2), function (err) {
        if (err) throw err;
    });
}

bot.command('writepoint', ctx => {
    writePointWW();
})

function writePointWW() {
    const date = moment().format('DDMMMMYYYY')
    fs.writeFile('pointww' + date + '.txt', JSON.stringify(totalplayerww, null, 2), function (err) {
        if (err) throw err;
    });
}
let grup = [
    {
        label: 'ina',
        no: wwinaGroup
    },
    {
        label: 'oot',
        no: ootInaGroup
    },
    {
        label: 'uno',
        no: unoinaGroup
    },
    {
        label: 'all'
    }
];
grup.forEach((element, index) => {
    bot.command('toa@' + element.label, ctx => {
        let jenis = ctx.message.text.substr(5, 3);
        console.log(jenis, 'woy')
        if (jenis === 'oot') {
            bot.telegram.sendMessage(element.no, ctx.message.text.substring(9));
        } else if (jenis === 'uno') {
            bot.telegram.sendMessage(element.no, ctx.message.text.substring(9));
        } else if (jenis === 'all') {
            bot.telegram.sendMessage(wwinaGroup, ctx.message.text.substring(9));
            bot.telegram.sendMessage(unoinaGroup, ctx.message.text.substring(9));
            bot.telegram.sendMessage(ootInaGroup, ctx.message.text.substring(9));
        } else if (jenis === 'ina') {
            bot.telegram.sendMessage(wwinaGroup, ctx.message.text.substring(9));
        }
    })
});

bot.command('shio', ctx => {
    if (ctx.chat.type === 'private') {
        let shio = ctx.message.text.split(" ")[1];
        try {
            userEventWW.forEach(element => {
                if (element.id === ctx.from.id) {
                    element.username = ctx.from.username;
                    element.name = ctx.from.first_name + ' ' + (ctx.from.last_name ? ctx.from.last_name : '');
                    element.shio = shio;
                }
            });
            totalplayerww.forEach(element => {
                if (element.id === ctx.from.id) {
                    element.name = ctx.from.username;
                    element.display = ctx.from.first_name + ' ' + (ctx.from.last_name ? ctx.from.last_name : '');
                }
            })

            ctx.reply('data berhasil diperbarui')
        } catch (error) {
            ctx.reply('terdapat kesalahan')
        }
        console.log(shio, 'koplak')
    }
})

bot.command('writepeserta', ctx => {
    writePeserta();
})

bot.command('dnevent', ctx => {
    let res = '';
    if (userEventWW.some(el => el.id === ctx.from.id)) {
        userEventWW.forEach(element => {
            if (ctx.from.id === element.id) {
                allShio.forEach(shio => {
                    if (element.shio === shio.shio) {
                        res = shio.icon + ' ' + element.name + ' ' + ' #AlexisLunarParty';
                    }
                });
            }
        });
        ctx.reply(res);
    } else {
        ctx.reply('belom daftar event')
    }
})

bot.command('pesertaww', ctx => {
    let res = '';
    if (userEventWW.length > 0) {
        userEventWW.forEach((element, index) => {
            res = res + '\n' + (index + 1) + '. ' + element.name + ' ( @' + element.username + ' ) ' + '\n'
            res = res + '    shio : ' + element.shio + '\n'
        });

        ctx.reply(res)
    } else {
        ctx.reply('kosong')
    }
})

function writeUser() {
    fs.writeFile('userina.txt', JSON.stringify(user, null, 2), function (err) {
        if (err) throw err;
    });
}
bot.catch((err, ctx) => {
    console.log(`Ooops, encountered an error for ${ctx.updateType}`, err)
})
bot.command('getdoc', (ctx) => {
    writeUser();
})

bot.command('getadmin', (ctx) => {
    fs.writeFile('adminina.txt', JSON.stringify(admin, null, 2), function (err) {
        if (err) throw err;
    });
})

bot.command('bacaadmin', ctx => {
    fs.readFile('adminina.txt', 'utf8', (err, data) => {
        admin = JSON.parse(data);
    });
})

bot.command('bacauser', (ctx) => {
    getData();
})

bot.command('cek', ctx => {
    console.log(user, 'asikin')
})

bot.command('questww', ctx => {
    if (ctx.chat.id === wwinaGroup && admin.some(el => el.id === ctx.from.id)) {
        let temp = ctx.message.text.split('questww')[1].split(" ")[1].replace('@', '').trim();
        let point = parseInt(ctx.message.text.split('questww')[1].split(" ")[2]);
        totalplayerww.forEach(element => {
            if (element.name === temp) {
                element.quest += point;
                element.totalPoint += point;
                console.log(element, 'lawak')
            }
        });
        console.log(point, 'nya', temp)
    } else {
        ctx.reply('anda bukan admin')
    }
})

bot.command('winnerww', ctx => {
    totalplayerww.sort((a, b) => {
        return a.totalPoint - b.totalPoint;
    })
    totalplayerww.reverse();
    totalplayerww.forEach((el, index) => {
        if(index < 3){
            ctx.reply('Juara ' + (index + 1) + ' \n' + el.display + '(@' + el.name + ') \nTotal Point : ' + el.totalPoint )
        }
    });
})

bot.command('afkww', ctx => {
    if (ctx.chat.id === wwinaGroup && admin.some(el => el.id === ctx.from.id)) {
        let temp = ctx.message.text.split('afkww')[1].replace('@', '').trim();
        totalplayerww.forEach(element => {
            if (element.name === temp) {
                element.afk += 1;
                element.totalPoint -= 2;
            }
        });
    } else {
        ctx.reply('anda bukan admin')
    }
})

bot.command('getwwstat', ctx => {
    let result = '';
    if (totalplayerww.length > 1) {
        totalplayerww.sort((a, b) => {
            return a.totalPoint - b.totalPoint;
        })
        totalplayerww.reverse();
        totalplayerww.forEach(element => {
            let kalimat;
            let data = [];
            let caption = '<b> ~~~ ' + element.display + ' ( ' + element.name + ' )' + ' ~~~ </b>';
            element.data.forEach((match, index) => {
                kalimat = '\n' + '<b>Match ' + (index + 1) + '</b>\n' + '<b>Status : </b>' + match.role + ' ' + (match.win ? ' <b>Menang</b> ' : ' <b>Kalah</b> ') + ' & ' + (match.survive ? '<b>Hidup</b>' : '<b>Mati</b>') + '\n' + '<b>Point : ' + match.point + '</b>' + '\n'
                data.push(kalimat);
            });
            caption = caption + data.toString().replace(',', '') + '<b>AFK : ' + element.afk + '</b>\n' + '<b>Total Point : ' + element.totalPoint + '</b>' + '\n';
            result = result + '\n' + caption;
            result = result.replace(',', '');
        });
        bot.telegram.sendMessage(ctx.chat.id, result, {
            parse_mode: 'HTML'
        })
    } else {
        ctx.reply('stat kosong')
    }

})

bot.command('getmystat', ctx => {
    if (ctx.chat.type === 'private') {
        let result = '';
        if (totalplayerww.length > 1) {
            totalplayerww.forEach(element => {

                if (element.id === ctx.from.id) {
                    let kalimat;
                    let data = [];
                    let caption = '<b> ~~~ ' + element.display + ' ( ' + element.name + ' )' + ' ~~~ </b>';
                    element.data.forEach((match, index) => {
                        kalimat = '\n' + '<b>Match ' + (index + 1) + '</b>\n' + '<b>Status : </b>' + match.role + ' ' + (match.win ? ' <b>Menang</b> ' : ' <b>Kalah</b> ') + ' & ' + (match.survive ? '<b>Hidup</b>' : '<b>Mati</b>') + '\n' + '<b>Point : ' + match.point + '</b>' + '\n'
                        data.push(kalimat);
                    });
                    caption = caption + data.toString().replace(',', '') + '<b>AFK : ' + element.afk + ' kali</b>\n' + '<b>Quest : +' + element.quest + '</b>\n' + '<b>Total Point : ' + element.totalPoint + '</b>' + '\n';
                    result = result + '\n' + caption;
                    result = result.replace(',', '');
                }
            });
            bot.telegram.sendMessage(ctx.chat.id, result, {
                parse_mode: 'HTML'
            })
        } else {
            ctx.reply('stat anda kosong')
        }
    }

})

const getData = () => {
    return new Promise(resolve => {
        fs.readFile('userina.txt', 'utf8', (err, data) => {
            user = JSON.parse(data)
            resolve(true);
        });
    })
}

bot.command('panggil', async (ctx) => {
    try {
        if (togglePanggil === 0) {
            togglePanggil = 1;
            if (user.length < 10) {
                await getData().then(() => {
                    var array = singleToMulti(user, 4);
                    for (let i = 0; i < array.length; i++) {
                        let b = '';
                        array[i].forEach(nama => {
                            b += nama.username + ' '
                        });
                        setTimeout(() => {
                            ctx.reply(b)
                            if (i === array.length - 1) {
                                togglePanggil = 0;
                            }
                        }, 700 * i);
                    }
                }).catch((e) => {
                    ctx.reply('rusak '.e)
                });
            }
            else {
                var array = singleToMulti(user, 4);
                for (let i = 0; i < array.length; i++) {
                    let b = '';
                    array[i].forEach(nama => {
                        b += nama.username + ' '
                    });
                    setTimeout(() => {
                        ctx.reply(b)
                        if (i === array.length - 1) {
                            togglePanggil = 0;
                        }
                    }, 700 * i);
                }
            }
        }
    } catch (error) {
        ctx.reply('yah rusak'.error)
    }
})

function singleToMulti(array, elementsPerSubArray) {
    var multiArray = [], i, j;
    for (i = 0, j = -1; i < array.length; i++) {
        if (i % elementsPerSubArray === 0) {
            j++;
            multiArray[j] = [];
        }
        multiArray[j].push(array[i]);
    }
    return multiArray;
}

bot.command('truth', (ctx) => {
    const rawTruth = fs.readFileSync("./listTruth.json", "utf8")
    const data = JSON.parse(rawTruth);
    ctx.reply(data[Math.floor(Math.random() * data.length)]);
})

bot.command('dare', (ctx) => {
    const rawDare = fs.readFileSync("./listDare.json", "utf8")
    const data = JSON.parse(rawDare);
    ctx.reply(data[Math.floor(Math.random() * data.length)]);
})

// bot.command('menu@kakayebot', (ctx) => {
//     bot.telegram.sendMessage(ctx.chat.id, 'Menu', {
//         reply_markup: {
//             inline_keyboard: [
//                 [
//                     {
//                         text: 'lihat pacar',
//                         callback_data: 'pacar'
//                     },
//                 ]
//             ]
//         }
//     })
// })

let stateTod = 0;
let ikutanTod = [];

bot.command('startTOD', (ctx) => {
    if (stateTod == 0) {
        stateTod = 1
        ctx.reply('Memulai sesi TOD')
    } else {
        ctx.reply('TOD sedang berlangsung')
    }
})

bot.command('joinTOD', (ctx) => {
    if (stateTod == 1) {
        ikutanTod.push(ctx.from.username)
        ctx.reply('yang main TOD ' + ikutanTod)
    } else if (stateTod == 2) {
        ctx.reply('sesi ditutup, harap buka /openTOD')
    } else {
        ctx.reply('tidak ada sesi, harap mulai /startTOD')
    }
})

bot.command('closeTOD', (ctx) => {
    if (stateTod == 1) {
        stateTod = 2
        ctx.reply('sesi ditutup, tidak ada yang bisa join lagi')
    }
})
bot.command('openTOD', (ctx) => {
    if (stateTod == 2) {
        stateTod = 1
        ctx.reply('sesi dibuka, silahkan /joinTOD')
    } else if (stateTod == 0) {
        ctx.reply('tidak ada sesi, harap mulai /startTOD')
    }
})
bot.command('clearTOD', (ctx) => {
    if (stateTod != 0) {
        stateTod = 0
        ikutanTod = []
        ctx.reply('sesi berakhir, silahkan /startTOD untuk memulai kembali')
    } else if (stateTod == 0) {
        ctx.reply('tidak ada sesi, harap mulai /startTOD')
    }
})

let rahasia = '';
bot.action('gembok', (ctx) => {
    if (ikutanTod.includes(ctx.from.username)) {
        ctx.answerCbQuery(rahasia, true)
    } else {
        ctx.answerCbQuery('lu kaga maen', true)
    }
})

bot.on('inline_query', (ctx) => {
    rahasia = ctx.inlineQuery.query;
    bot.telegram.answerInlineQuery(ctx.inlineQuery.id, [
        {
            type: 'article',
            id: "1",
            title: "Kirim",
            input_message_content:
            {
                message_text: 'Pesan Rahasia'
            },
            reply_markup: {
                inline_keyboard: [
                    [
                        {
                            text: 'buka pesan',
                            callback_data: 'gembok'
                        },
                    ]
                ]
            }
        }
    ])
})

bot.command('wait', ctx => {
    durasi = parseInt(ctx.message.text.substring(5)) * 1000;
    ctx.reply('Hitung Mundur Dimulai ' + (durasi / 1000) + ' detik');
    timeCount = setTimeout(() => {
        ctx.reply('TIMEOUT, GUGUR!!');
    }, durasi);
})

bot.command('clearwait', ctx => {
    clearTimeout(timeCount);
    ctx.reply('Hitung mundur berhenti');
});

bot.command('tempban', ctx => {
    if (ctx.chat.id === ootInaGroup || ctx.chat.id === wwinaGroup) {
        if (admin.some(el => el.id === ctx.from.id)) {
            bot.telegram.kickChatMember(ctx.chat.id, ctx.message.reply_to_message.from.id);
            ctx.reply('tempban berhasil')
            let time = ctx.message.text.split(' ');
            setTimeout(() => {
                bot.telegram.unbanChatMember(ctx.chat.id, ctx.message.reply_to_message.from.id);
            }, parseInt(time[1]) * 1000 * 60);
        } else {
            ctx.reply('anda bukan admin')
        }
    }
})

bot.command('ban', ctx => {
    if (ctx.chat.id === ootInaGroup || ctx.chat.id === wwinaGroup) {
        if (admin.some(el => el.id === ctx.from.id)) {
            bot.telegram.kickChatMember(ctx.chat.id, ctx.message.reply_to_message.from.id);
            ctx.reply('ban berhasil')
        } else {
            ctx.reply('anda bukan admin')
        }
    }
})

bot.command('untag', ctx => {
    if (!untag.some(el => el.id === ctx.from.id)) {
        untag.push({
            id: ctx.from.id,
            username: ctx.from.username
        })
        user = user.filter(el => el.id !== ctx.from.id);
        writeUser();
        fs.writeFile('untag.txt', JSON.stringify(untag, null, 2), function (err) {
            if (err) throw err;
        });
    }
})

bot.command('listadmin', ctx => {
    if (admin.length > 0) {
        admin.forEach((element, index) => {
            ctx.reply((index + 1) + ' @' + element.name);
        });
    }
})


bot.command('shiolist', (ctx) => {


    let result;
    allShio.forEach(element => {
        if (result) {
            result = result + '\n' + element.shio;
        } else {
            result = element.shio;
        }
    });

    bot.telegram.sendMessage(ctx.chat.id, result, {
        parse_mode: 'HTML'
    })
})

bot.command('/help@eloquentbot', ctx => {
    let result = '';
    commandList.forEach(element => {
        result = result + element.type + '\n';
        element.data.forEach(data => {
            result = result + data.cmd + '  =>  ' + data.ket + '\n'
        });
        result = result + '\n'
    });
    bot.telegram.sendMessage(ctx.chat.id, result, {
        parse_mode: 'HTML'
    })
})

bot.command('sync', ctx => {
    try {
        userEventWW.forEach(element => {
            if (element.id === ctx.from.id) {
                element.username = ctx.from.username;
                element.name = ctx.from.first_name + ' ' + (ctx.from.last_name ? ctx.from.last_name : '');
            }
        });
        totalplayerww.forEach(element => {
            if (element.id === ctx.from.id) {
                element.name = ctx.from.username;
                element.display = ctx.from.first_name + ' ' + (ctx.from.last_name ? ctx.from.last_name : '');
            }
        })

        ctx.reply('data berhasil diperbarui')
    } catch (error) {
        ctx.reply('terdapat kesalahan')
    }

})

bot.command('writeww', ctx => {
    if (ctx.chat.id === wwinaGroup && admin.some(el => el.id === ctx.from.id)) {

        matchww.push(ctx.message.reply_to_message.text);
        const tempww = ctx.message.reply_to_message.text.split("\n");
        let tempplayer = [];
        ctx.message.reply_to_message.entities.forEach(element => {
            if (element.user) {
                tempplayer.push(element.user);
            }
        });
        matchplayerww = [];
        for (let i = 0; i < (tempww.length - 2); i++) {
            if (i != 0) {
                matchplayerww.push({
                    raw: tempww[i]
                });
            }
        }
        tempplayer.forEach((element, index) => {
            matchplayerww[index].user = element;
        });
        matchplayerww.forEach((element, index) => {
            let win;
            let survive;
            let tempPoint = 0;
            if (element.raw.includes("Tewas")) {
                survive = false;
            } else if (element.raw.includes("Masih Hidup")) {
                survive = true;
            }
            if (element.raw.endsWith("Kalah")) {
                win = false;
            } else if (element.raw.endsWith("Menang")) {
                win = true;
            }

            if (win && survive) {
                tempPoint = 4
            } else if (win && !survive) {
                tempPoint = 2
            } else if (!win && survive) {
                tempPoint = 3
            } else if (!win && !survive) {
                tempPoint = 1
            }

            if (totalplayerww.some(e => e.id === matchplayerww[index].user.id)) {
                totalplayerww.forEach(el => {
                    if (el.id === matchplayerww[index].user.id) {
                        let temprole = element.raw.split(" - ")[element.raw.split(" - ").length - 1];
                        if (temprole.endsWith('Kalah')) {
                            temprole = temprole.split("Kalah")[0];
                        } else if (temprole.endsWith('Menang')) {
                            temprole = temprole.split("Menang")[0];
                        }
                        el.data.push({
                            win: win,
                            survive: survive,
                            role: temprole,
                            point: tempPoint,
                        })
                        let totalPointTemp = 0;
                        el.data.forEach(point => {
                            totalPointTemp += point.point;
                        });
                        el.totalPoint = totalPointTemp;
                    }
                });
            } else {
                let temprole = element.raw.split(" - ")[element.raw.split(" - ").length - 1];
                if (temprole.endsWith('Kalah')) {
                    temprole = temprole.split("Kalah")[0];
                } else if (temprole.endsWith('Menang')) {
                    temprole = temprole.split("Menang")[0];
                }
                totalplayerww.push({
                    id: matchplayerww[index].user.id,
                    display: matchplayerww[index].user.first_name + ' ' + (matchplayerww[index].user.last_name ? matchplayerww[index].user.last_name : ''),
                    name: matchplayerww[index].user.username,
                    totalPoint: tempPoint,
                    afk: 0,
                    quest: 0,
                    data:
                        [
                            {
                                win: win,
                                survive: survive,
                                role: temprole,
                                point: tempPoint
                            }
                        ]
                })
            }
            writePointWW();
        });
    } else {
        ctx.reply('anda bukan admin')
    }
})

bot.on('text', async (ctx) => {
    // eventUno(ctx);

    if (ctx.chat.id === admininaGroup) {
        if (admin.some(el => el.id === ctx.from.id)) {

        } else {
            admin.push({
                name: ctx.from.username,
                id: ctx.from.id
            })
        }
    } else {
        grupUX(ctx);
        inaReplyAdmin(ctx, 'text');
        giveCaptionToMusic(ctx);
        if (ctx.chat.id === receptionistInaGroup || ctx.chat.id === ootInaGroup || ctx.chat.id === belajarGroup || ctx.chat.id === unoinaGroup || ctx.chat.id === wwinaGroup) {
            if (toggleuntag === 0) {
                await readUntag();
            }
            if (!untag.some(el => el.id === ctx.message.from.id)) { // kalo user untag maka tidak di add ke user
                if (user.length < 3) {
                    user.push({
                        id: ctx.message.from.id,
                        username: '@' + ctx.message.from.username
                    });
                } else {
                    if (user.some(el => el.id === ctx.message.from.id)) { // kalo user udah ada maka tidak di add ke user, melainkan hanya update usernamenya
                        user.forEach(element => {
                            if (element.id === ctx.message.from.id) {
                                element.username = '@' + ctx.message.from.username
                            }
                        });
                    }
                    else {
                        if(ctx.message.from.username !== undefined){
                            user.push({ // add user
                                id: ctx.message.from.id,
                                username: '@' + ctx.message.from.username
                            });
                        }
                    }
                }
            }
        }
    }
})

bot.command('bacauntag', ctx => {
    readUntag();
})



function readUntag() {
    return new Promise(resolve => {
        fs.readFile('untag.txt', 'utf8', (err, data) => {
            untag = JSON.parse(data);
            toggleuntag = 1;
            resolve(true);
        });
    })
}

bot.use(ctx => {
    if (ctx.channelPost) {
        if (ctx.channelPost.chat.id === belajarChannel) {  // testing pin message
            bot.telegram.sendMessage(belajarGroup, ctx.channelPost.text);
        }
        if (ctx.channelPost.chat.id === inaChannel) {
            if (ctx.channelPost.text) {
                bot.telegram.sendMessage(unoinaGroup, ctx.channelPost.text)
                bot.telegram.sendMessage(wwinaGroup, ctx.channelPost.text).then(e => {
                    bot.telegram.pinChatMessage(wwinaGroup, e.message_id)
                })
            } else if (ctx.channelPost.animation) {
                bot.telegram.sendAnimation(unoinaGroup, ctx.channelPost.animation.file_id)
                bot.telegram.sendAnimation(wwinaGroup, ctx.channelPost.animation.file_id).then(e => {
                    bot.telegram.pinChatMessage(wwinaGroup, e.message_id)
                })
            } else if (ctx.channelPost.sticker) {
                bot.telegram.sendSticker(unoinaGroup, ctx.channelPost.sticker.file_id)
                bot.telegram.sendSticker(wwinaGroup, ctx.channelPost.sticker.file_id).then(e => {
                    bot.telegram.pinChatMessage(wwinaGroup, e.message_id)
                })
            } else if (ctx.channelPost.photo) {
                bot.telegram.sendPhoto(unoinaGroup, ctx.channelPost.photo[0].file_id,
                    {
                        caption: ctx.channelPost.caption,
                        caption_entities: ctx.channelPost.caption_entities
                    })
                bot.telegram.sendPhoto(wwinaGroup, ctx.channelPost.photo[0].file_id,
                    {
                        caption: ctx.channelPost.caption,
                        caption_entities: ctx.channelPost.caption_entities
                    }).then(e => {
                        bot.telegram.pinChatMessage(wwinaGroup, e.message_id)
                    })
            } else if (ctx.channelPost.audio) {
                bot.telegram.sendAudio(unoinaGroup, ctx.channelPost.audio.file_id,
                    {
                        caption: ctx.channelPost.caption,
                        caption_entities: ctx.channelPost.caption_entities
                    })
                bot.telegram.sendAudio(wwinaGroup, ctx.channelPost.audio.file_id,
                    {
                        caption: ctx.channelPost.caption,
                        caption_entities: ctx.channelPost.caption_entities
                    }).then(e => {
                        bot.telegram.pinChatMessage(wwinaGroup, e.message_id)
                    })
                bot.telegram.chat
            }
        }
    }
})

bot.on('sticker', (ctx) => {
    inaReplyAdmin(ctx, 'sticker');
})

function giveCaptionToMusic(ctx) {
    if (ctx.message.reply_to_message) {
        if (ctx.chat.id === belajarGroup) {
            if (ctx.message.reply_to_message.audio) {
                musik = ctx.message.reply_to_message.audio.file_id;
                bot.telegram.sendAudio(ctx.chat.id, musik, { caption: '<i>' + ctx.message.text + '</i>', parse_mode: 'HTML' })
            }
        }
    }
}

function grupUX(ctx) {
    if (ctx.chat.id === greatdayUX) {
        if (ctx.message.text == 'AR') {
            bot.telegram.sendMessage(ctx.chat.id, 'Yuk join meeting ' + timUX, {
                reply_to_message_id: 36499,
            });
        }
    }
}

function inaReplyAdmin(ctx, type) {
    privateChatSendToInaAdmin(ctx, type);
    if (ctx.chat.id === receptionistInaGroup) {
        if (ctx.message.reply_to_message && ctx.message.reply_to_message.from.username === 'kakayebot' && !ctx.message.reply_to_message.audio) {
            let tempPesan = ctx.message.reply_to_message.text
            let awal = tempPesan.indexOf('@')
            let akhir = tempPesan.indexOf(')')
            let usernamenya = tempPesan.slice(awal + 1, akhir);
            privateRoomId.forEach(element => {
                if (element.username === usernamenya) {
                    if (type === 'sticker') {
                        bot.telegram.sendSticker(element.chatId, ctx.message.sticker.file_id);
                    }
                    else if (type === 'text') {
                        bot.telegram.sendMessage(element.chatId, ctx.message.text);
                    }
                }
            });
        }
    }
}

let userEventWW = [];



function privateChatSendToInaAdmin(ctx, type) {
    if (ctx.chat.type === 'private') {

        if (!userEventWW.some(el => el.id === ctx.chat.id)) {
            if (ctx.message.text === '/daftar') {
                // userEventWW.push({
                //     id: ctx.chat.id,
                //     username: ctx.from.username,
                //     name: ctx.from.first_name + ' ' + (ctx.from.last_name ? ctx.from.last_name : ''),
                //     session: 0
                // })

                // bot.telegram.sendMessage(ctx.chat.id, 'Hi, Shio kamu apa?', {
                //     parse_mode: 'HTML'
                // });
                ctx.reply('dah tutup pendaftarannya')
            }
            else {
                if (privateRoomId.length < 1) {
                    privateRoomId.push({
                        chatId: ctx.chat.id,
                        username: ctx.from.username
                    })
                } else {
                    if (privateRoomId.some(el => el.chatId === ctx.chat.id)) { }
                    else {
                        privateRoomId.push({
                            chatId: ctx.chat.id,
                            username: ctx.from.username
                        })
                    }
                }
                const sender = ctx.from.first_name + ' ' + (ctx.from.last_name ? ctx.from.last_name : '');
                const uname = '@' + ctx.from.username;
                if (type === 'text') {
                    const pesan = ctx.message.text;
                    bot.telegram.sendMessage(receptionistInaGroup, 'From : ' + sender + ' (' + uname + ')' + '\n' + pesan, {
                        parse_mode: 'HTML'
                    });
                    // bot.telegram.sendMessage(wwinaGroup, pesan, {
                    //     parse_mode: 'HTML'
                    // });
                    // bot.telegram.sendMessage(ootInaGroup, pesan, {
                    //     parse_mode: 'HTML'
                    // });
                    // bot.telegram.sendMessage(unoinaGroup, pesan, {
                    //     parse_mode: 'HTML'
                    // });
                    // bot.telegram.sendMessage(receptionistInaGroup, pesan);
                } else if (type === 'sticker') {
                    bot.telegram.sendMessage(receptionistInaGroup, 'From : ' + sender + ' (' + uname + ')' + '\n');
                    // bot.telegram.sendSticker(belajarGroup, ctx.message.sticker.file_id);;
                }
            }
        } else {
            const found = userEventWW.find(el => el.id === ctx.chat.id);
            if (found.session === 0) {
                bot.telegram.sendMessage(ctx.chat.id, 'Selamat, kamu telah terdaftar. \nJika ada perubahan display name atau username harap gunakan /sync di chatroom ini', {
                    parse_mode: 'HTML'
                });
                let shio = ctx.message.text.split(" ")[1].charAt(0).toUpperCase() + ctx.message.text.split(" ")[1].slice(1);
                found.shio = shio;
                found.session = 1;
                const res = 'Terdaftar : ' + found.name + ' ( @' + found.username + ' ) ' + ' ' + found.shio;
                writePeserta();
                bot.telegram.sendMessage(receptionistInaGroup, res, {
                    parse_mode: 'HTML'
                });
            } else if (found.session === 1) {
                if (privateRoomId.length < 1) {
                    privateRoomId.push({
                        chatId: ctx.chat.id,
                        username: ctx.from.username
                    })
                } else {
                    if (privateRoomId.some(el => el.chatId === ctx.chat.id)) { }
                    else {
                        privateRoomId.push({
                            chatId: ctx.chat.id,
                            username: ctx.from.username
                        })
                    }
                }
                const sender = ctx.from.first_name + ' ' + (ctx.from.last_name ? ctx.from.last_name : '');
                const uname = '@' + ctx.from.username;
                if (type === 'text') {
                    const pesan = ctx.message.text;
                    bot.telegram.sendMessage(receptionistInaGroup, 'From : ' + sender + ' (' + uname + ')' + '\n' + pesan, {
                        parse_mode: 'HTML'
                    });
                } else if (type === 'sticker') {
                    bot.telegram.sendMessage(receptionistInaGroup, 'From : ' + sender + ' (' + uname + ')' + '\n');
                    // bot.telegram.sendSticker(belajarGroup, ctx.message.sticker.file_id);;
                }

            }
        }
    }
}

bot.startPolling();

// boiling-taiga-73389
// https://boiling-taiga-73389.herokuapp.com/ | 
// https://git.heroku.com/boiling-taiga-73389.git

// heroku config:set --app boiling-taiga-73389 BOT_TOKEN='990476503:AAFMnVYpJv8h-3neyAZzYuMFVq5Q8qQe4Q4'
// heroku config:set --app boiling-taiga-73389 BOT_DOMAIN='https://boiling-taiga-73389.herokuapp.com/'