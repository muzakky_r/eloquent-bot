[
  {
    "id": 1053913978,
    "display": "🐇 Han #AlexisLunarParty ",
    "name": "machiyatooo",
    "totalPoint": 11,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": true,
        "role": "Cultist Hunter 💂 ",
        "point": 4
      },
      {
        "win": true,
        "survive": false,
        "role": "Cultist 👤❤️ ",
        "point": 2
      },
      {
        "win": true,
        "survive": false,
        "role": "Guardian Angel 👼 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist Hunter 💂 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Guardian Angel 👼 ",
        "point": 2
      }
    ]
  },
  {
    "id": 280878052,
    "display": "🐃 Schalea #AlexisLunarParty",
    "name": "abcdbgtL",
    "totalPoint": 10,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": true,
        "role": " Mason 👷 ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Anak Serigala 🐶 ",
        "point": 1
      },
      {
        "win": false,
        "survive": true,
        "role": "Hunter 🎯 ",
        "point": 3
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1692276216,
    "display": "🐓 xazayn #AlexisLunarParty ",
    "name": "leoxazayn",
    "totalPoint": 12,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": true,
        "role": "Monarch 👑 ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist Hunter 💂 ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "Detektif 🕵 ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Sandman 💤 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      }
    ]
  },
  {
    "id": 1100162452,
    "display": "🐕 ast #AlexisLunarParty ",
    "name": "Lalubagaimana",
    "totalPoint": 8,
    "afk": 1,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Guardian Angel 👼 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Terkutuk (Cursed) 😾 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "the WolfMan 👱🌚 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      }
    ]
  },
  {
    "id": 297588218,
    "display": "🐃 zakky #AlexisLunarParty ",
    "name": "kkybox",
    "totalPoint": 8,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱❤️ ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Alchemist 🍵 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Prowler 🦉 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 2
      },
      {
        "win": true,
        "survive": false,
        "role": "Penerawang (Seer) 👳 ",
        "point": 2
      }
    ]
  },
  {
    "id": 266560985,
    "display": "🐕 Tanjul #AlexisLunarParty ",
    "name": "malesinbanget",
    "totalPoint": 3,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Cultist 👤❤️ ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Mystic ☄️ ",
        "point": 1
      }
    ]
  },
  {
    "id": 1529496878,
    "display": "🐉 Aya #AlexisLunarParty ",
    "name": "ayyyea",
    "totalPoint": 9,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Detektif 🕵 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Mystic ☄️ ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "Si Ceroboh (Clumsy Guy) 🤕 ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "The Puppet Master 🕴 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1412871977,
    "display": "🐎 Yuli #AlexisLunarParty ",
    "name": "Lalalana7",
    "totalPoint": 8,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Harlot 💋 ",
        "point": 2
      },
      {
        "win": true,
        "survive": false,
        "role": "Martyr 🔰 ",
        "point": 2
      },
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1453028953,
    "display": "🐉 Eka #AlexisLunarParty ",
    "name": "ekayapu",
    "totalPoint": 1,
    "afk": 2,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1008823921,
    "display": "🐇 Jhin 🐧᭄ #AlexisLunarParty ",
    "name": "masable",
    "totalPoint": 4,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "the Wise Elder 📚 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": " Mason 👷 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1083250374,
    "display": "🐐 malaikat #AlexisLunarParty",
    "name": "letshavefuntogetherr",
    "totalPoint": 13,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "the WolfMan 👱🌚 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Terkutuk (Cursed) 😾 ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "the Beauty 💅❤️ ",
        "point": 4
      },
      {
        "win": true,
        "survive": true,
        "role": "Trickster Wolf 🐑 ",
        "point": 4
      },
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      }
    ]
  },
  {
    "id": 1141758056,
    "display": "🐖 Bondet #AlexisLunarParty ",
    "name": "Bondets",
    "totalPoint": 14,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": " Mason 👷 ",
        "point": 2
      },
      {
        "win": true,
        "survive": true,
        "role": "Cultist 👤 ",
        "point": 4
      },
      {
        "win": true,
        "survive": true,
        "role": "Wild Child 👶 ",
        "point": 4
      },
      {
        "win": true,
        "survive": false,
        "role": "the Alpha Wolf ⚡️ ",
        "point": 2
      },
      {
        "win": true,
        "survive": false,
        "role": "Cultist Hunter 💂 ",
        "point": 2
      }
    ]
  },
  {
    "id": 1593225694,
    "display": "🐐 A¢edia #AlexisLunarParty ",
    "name": "DosaMalas",
    "totalPoint": 5,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Cultist Hunter 💂 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Anak Serigala 🐶 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 2
      }
    ]
  },
  {
    "id": 750780315,
    "display": "🐃 Sewindu #AlexisLunarParty ",
    "name": "sewindu8",
    "totalPoint": 0,
    "afk": 1,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      }
    ]
  },
  {
    "id": 910520359,
    "display": "🐀 Dim #AlexisLunarParty ",
    "name": "dimasdik",
    "totalPoint": 5,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Harlot 💋 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Detektif 🕵 ",
        "point": 2
      }
    ]
  },
  {
    "id": 973197329,
    "display": "🐇 Rizky #AlexisLunarParty",
    "name": "Rizkyid21",
    "totalPoint": 2,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "the Pacifist ☮️ ",
        "point": 2
      }
    ]
  },
  {
    "id": 1135242132,
    "display": "🐇 Razel #AlexisLunarParty ",
    "name": "beribenkaa",
    "totalPoint": 12,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Pemabuk 🍻 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Sandman 💤 ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "Alchemist 🍵 ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Penerawang (Seer) 👳 ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "Alchemist 🍵 ",
        "point": 4
      }
    ]
  },
  {
    "id": 944998018,
    "display": "🐇 yakult #AlexisLunarParty ",
    "name": "yakultkult",
    "totalPoint": 5,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Beholder 👁 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Harlot 💋 ",
        "point": 2
      }
    ]
  },
  {
    "id": 1196934670,
    "display": "🐍 Morthy #AlexisLunarParty ",
    "name": "Morthynorthy",
    "totalPoint": 12,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      },
      {
        "win": true,
        "survive": true,
        "role": "Cultist 👤 ",
        "point": 4
      },
      {
        "win": true,
        "survive": true,
        "role": "Warga Desa 👱 ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1397724281,
    "display": "🐉 nadippp #AlexisLunarParty ",
    "name": "sknzdpn",
    "totalPoint": 12,
    "afk": 0,
    "quest": 2,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "the Alpha Wolf ⚡️ ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Pandai Besi (BlackSmith) ⚒ ",
        "point": 2
      },
      {
        "win": true,
        "survive": true,
        "role": "Prowler 🦉 ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Mystic ☄️ ",
        "point": 1
      }
    ]
  },
  {
    "id": 301339844,
    "display": "🧩 ",
    "name": "Zokiterbot",
    "totalPoint": 2,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Fool 🃏 ",
        "point": 2
      }
    ]
  },
  {
    "id": 1348397279,
    "display": "🔱 ᴊᴏᴋᴏ #Titanomakhia ",
    "name": "Jokomu0",
    "totalPoint": 6,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Penerawang (Seer) 👳 ",
        "point": 2
      },
      {
        "win": true,
        "survive": true,
        "role": "Kepala Desa (Mayor) 🎖 ",
        "point": 4
      }
    ]
  },
  {
    "id": 1475990751,
    "display": "🔱 Yun #Titanomakhia ",
    "name": "YuniInd1",
    "totalPoint": 4,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Arsonist 🔥 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1038021358,
    "display": "🐕 Ayam #AlexisLunarParty ",
    "name": "AyamGorengmu",
    "totalPoint": 5,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Pandai Besi (BlackSmith) ⚒ ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Penerawang (Seer) 👳 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Cultist Hunter 💂 ",
        "point": 2
      }
    ]
  },
  {
    "id": 263934088,
    "display": "🐃 anggun. #AlexisLunarParty ",
    "name": "anakmanis",
    "totalPoint": 3,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": " Mason 👷 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Pengkhianat 🖕 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1148025135,
    "display": "🐀 Kanin #AlexisLunarParty ",
    "name": "sapingky",
    "totalPoint": 10,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      },
      {
        "win": true,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "the Alpha Wolf ⚡️ ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "Hunter 🎯 ",
        "point": 4
      }
    ]
  },
  {
    "id": 675844458,
    "display": "🐓 Pram Arlert #AlexisLunarParty ",
    "name": "InformanTelegram",
    "totalPoint": 4,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Pengkhianat 🖕 ",
        "point": 2
      },
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      }
    ]
  },
  {
    "id": 687126873,
    "display": "🐒 Daisuke Kanbe #AlexisLunarParty ",
    "name": "Dichodesu",
    "totalPoint": 7,
    "afk": 0,
    "quest": 1,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Cupid 🏹 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "The Puppet Master 🕴 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Lycan 🐺🌝 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Lycan 🐺🌝 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1291079866,
    "display": "🐐 aiss #AlexisLunarParty",
    "name": "aissssssssssssss",
    "totalPoint": 9,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Lycan 🐺🌝 ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "the Wise Elder 📚 ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Martyr 🔰 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Pandai Besi (BlackSmith) ⚒ ",
        "point": 2
      }
    ]
  },
  {
    "id": 1237873555,
    "display": "🐅 kimuna #AlexisLunarParty ",
    "name": "munanah",
    "totalPoint": 3,
    "afk": 1,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Lycan 🐺🌝 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 2
      },
      {
        "win": true,
        "survive": false,
        "role": " Mason 👷 ",
        "point": 2
      }
    ]
  },
  {
    "id": 283594304,
    "display": "🐃 Afril #AlexisLunarParty ",
    "name": "Purplepepper",
    "totalPoint": 6,
    "afk": 1,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Guardian Angel 👼❤️ ",
        "point": 2
      },
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Alchemist 🍵 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1331866935,
    "display": "🐒 Gonu 🐻 #AlexisLunarParty",
    "name": "Onlylevixx",
    "totalPoint": 5,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Serial Killer 🔪 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Anak Serigala 🐶 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Warga Desa 👱 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Oracle 🌀 ",
        "point": 1
      }
    ]
  },
  {
    "id": 901343996,
    "display": "🐃 Kaleyo #AlexisLunarParty ",
    "name": "bebekaleyo",
    "totalPoint": 4,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Thief 😈 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Arsonist 🔥 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      }
    ]
  },
  {
    "id": 285041793,
    "display": "🐇 Eliavina #AlexisLunarParty ",
    "name": "addictedtuyu",
    "totalPoint": 1,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Anak Serigala 🐶 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1424444949,
    "display": "🐉 Kirei #AlexisLunarParty ",
    "name": "sunnshinees",
    "totalPoint": 13,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "Cultist 👤 ",
        "point": 4
      },
      {
        "win": true,
        "survive": false,
        "role": "Martyr 🔰 ",
        "point": 2
      },
      {
        "win": true,
        "survive": true,
        "role": "the Alpha Wolf ⚡️❤️ ",
        "point": 4
      },
      {
        "win": true,
        "survive": false,
        "role": "the Beauty 💅❤️ ",
        "point": 2
      }
    ]
  },
  {
    "id": 1118388082,
    "display": "🐕 Nas #AlexisLunarParty ",
    "name": "nanasasem",
    "totalPoint": 9,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Mystic ☄️ ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 2
      },
      {
        "win": true,
        "survive": true,
        "role": "the Pacifist ☮️ ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 1
      }
    ]
  },
  {
    "id": 394631355,
    "display": "🐍 doo #AlexisLunarParty ",
    "name": "paris_gun",
    "totalPoint": 5,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Beholder 👁 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Monarch 👑 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Penerawang (Seer) 👳 ",
        "point": 2
      }
    ]
  },
  {
    "id": 1467037151,
    "display": "🔱 Ryan #Titanomakhia ",
    "name": "Rynsa1",
    "totalPoint": 5,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "the WolfMan 👱🌚 ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "Monarch 👑 ",
        "point": 4
      }
    ]
  },
  {
    "id": 1216979041,
    "display": "🐅 Cleo #AlexisLunarParty ",
    "name": "Cleopartyz",
    "totalPoint": 9,
    "afk": 0,
    "quest": 2,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Cultist Hunter 💂 ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "Thief 😈❤️ ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Pemabuk 🍻 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      }
    ]
  },
  {
    "id": 263348787,
    "display": "🐃 Az #AlexisLunarParty",
    "name": "gosahbawel",
    "totalPoint": 6,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Oracle 🌀 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": "Pengkhianat 🖕 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Harlot 💋 ",
        "point": 1
      },
      {
        "win": true,
        "survive": false,
        "role": " Mason 👷 ",
        "point": 2
      }
    ]
  },
  {
    "id": 1099582331,
    "display": "🐍 Maria #AlexisLunarParty ",
    "name": "hauareyou",
    "totalPoint": 7,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": false,
        "survive": false,
        "role": "Pemabuk 🍻 ",
        "point": 1
      },
      {
        "win": true,
        "survive": true,
        "role": "Penerawang (Seer) 👳 ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Serigala 🐺 ",
        "point": 1
      }
    ]
  },
  {
    "id": 1262900704,
    "display": "bry ",
    "name": "boostermod",
    "totalPoint": 2,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 2
      }
    ]
  },
  {
    "id": 1464633350,
    "display": "🐕 Elan #AlexisLunarParty ",
    "name": "morningbomb",
    "totalPoint": 3,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Cultist 👤 ",
        "point": 2
      },
      {
        "win": false,
        "survive": false,
        "role": "Cultist Hunter 💂 ",
        "point": 1
      }
    ]
  },
  {
    "id": 752091262,
    "display": "🐅 zeedny #AlexisLunarParty ",
    "name": "Zidneyara",
    "totalPoint": 10,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": true,
        "role": "Cultist 👤 ",
        "point": 4
      },
      {
        "win": true,
        "survive": true,
        "role": "Fool 🃏 ",
        "point": 4
      },
      {
        "win": false,
        "survive": false,
        "role": "Detektif 🕵 ",
        "point": 1
      },
      {
        "win": false,
        "survive": false,
        "role": "Thief 😈 ",
        "point": 1
      }
    ]
  },
  {
    "id": 376256481,
    "display": "🐐 Arsene Lupin #AlexisLunarParty",
    "name": "ArseneLupinn",
    "totalPoint": 8,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": true,
        "role": "Gunner 🔫 ",
        "point": 4
      },
      {
        "win": true,
        "survive": false,
        "role": "the Beauty 💅❤️ ",
        "point": 2
      },
      {
        "win": true,
        "survive": false,
        "role": "Serigala 🐺❤️ ",
        "point": 2
      }
    ]
  },
  {
    "id": 1275695136,
    "display": "!ndah ",
    "name": "indahyuliaaa",
    "totalPoint": 2,
    "afk": 0,
    "quest": 0,
    "data": [
      {
        "win": true,
        "survive": false,
        "role": "Gunner 🔫 ",
        "point": 2
      }
    ]
  }
]